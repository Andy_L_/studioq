$(document).foundation();

$('.go-down').on('touchstart click', 'a', function (e) {
  e.preventDefault();
  var targetScroll = $(this).data('target-scroll');
  if (window.innerWidth <= 520) {
    return;
  }
  var scrollTo = $("#"+targetScroll+"").offset().top;
  $('html, body').animate({scrollTop: scrollTo > 0 ? scrollTo : 1000}, 1000);
});

//$('#top-nav').on('touchstart click', '.menu-icon', function (e) {
//  e.preventDefault();
//  $("#responsive-menu").toggleClass("openMenu");
//});